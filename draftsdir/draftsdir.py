"""
This simple plugin allows you to define a directory under your datadir
that contains draft blog entries. Draft entries will not be displayed
on the blog by default. To display drafts add a 'drafts' query
parameter to the end of the page URL. For example:

    http://my.site.com/blog/?drafts
or
    http://my.site.com/blog/?other=arg&drafts


To install draftsdir, do the following:

  1. Copy draftsdir.py into your plugins directory.
  2. Add "draftsdir" to the load_plugins list variable in
     config.py. It's probably best to have it as the first plugin in
     the list. It uses a hack which might affect other plugins.
  3. Set the "drafts_directory" option in configuration. This should
     be a directory that's a child of your datadir and should be
     specified as a relative path. 

A single configuration variable is required. The other is optional.

 drafts_directory 
    datatype:       string
    default value:  None - plugin will be disabled if not set
    description:    Directory used for draft entries (see above)
    example:        py['drafts_directory'] = 'drafts'
    
 drafts_arg 
    datatype:       string
    default value:  "drafts"
    description:    The name of the URL query argument which will
                    activate viewing of drafts. Changing this to a 
                    hard to guess value will make it more difficult
                    for others to find your drafts.
    example:        py['drafts_arg'] = 'view_drafts'

This plugin was inspired by the "drafts" plugin.


Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Copyright 2009 Menno Smits
"""

__author__      = "Menno Smits - menno at freshfoo dot com"
__version__     = "version 0.1 2009-06-25"
__url__         = "http://freshfoo.com/code/"
__description__ = "Defines a directory for normally hidden draft entries"


def cb_start(args):
    request = args['request']
    config = request.getConfiguration()
    drafts_dir = config.get('drafts_directory', None)
    if not drafts_dir:
        return
    drafts_arg = config.get('drafts_arg', 'drafts')
    query = request.getHttp().get('QUERY_STRING', '')
    if not (query == drafts_arg or '&'+drafts_arg in query):
        config['ignore_directories'].append(drafts_dir)

    
