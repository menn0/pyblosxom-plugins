"""
Summary
=======

The plugin adds a"break" directive which allows articles written in
reStructured Text (reST) to interact with the readmore plugin.

Configuration
=============

1. copy rst_break.py into your plugins directory
2. enable it by adding ``rst_break`` to the ``py['load_plugins']`` list 
   variable in your config.py file (probably located in a cgi-bin directory).
   It should probably be added before the rst plugin.

The plugin has no configuration options of it's own. It uses the
readmore_breakpoint configuration option used by the readmore plugin.

Usage
=====

To add a "read more..." breakpoint into a reST based article used the
break directive like this:

  Introductory text goes here.

  .. break::

  The rest of the text goes here.

"""

__author__      = "Menno Smits - menno at freshfoo dot com"
__version__     = "version 0.1 2010-01-30"
__url__         = "http://freshfoo.com/code/"
__description__ = "Add a directive for reST articles so that they can use the readmore plugin"

from docutils import nodes
from docutils.parsers.rst import directives, Directive

def cb_start(args):
    request = args['request']
    config = request.getConfiguration()
    breakpoint = config.get('readmore_breakpoint', None)

    class Break(Directive):
        """
        Transform a break directive (".. break::") into the text that
        the Pyblosxom readmore plugin looks for. This allow article written
        in reST to use this plugin.
        """

        required_arguments = 0
        optional_arguments = 0
        final_argument_whitespace = True
        has_content = False

        def run(self):
            return [nodes.raw('', breakpoint+'\n', format='html')]

    directives.register_directive('break', Break)

